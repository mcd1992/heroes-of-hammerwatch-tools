struct resource_loader_class {
  void* unk_ptr0;       // 0x00
  void* unk_ptr1;       // 0x08
  void* unk_ptr2;       // 0x10
  void* unk_ptr3;       // 0x18
  void* unk_ptr4;       // 0x20
  void* unk_ptr5;       // 0x28 - This gets checked if 0 in resource_loader_init
  void* unk_ptr6;       // 0x30
  void* unk_spacer[13]; // 0x38
  void* unk_ptr7;       // 0xA0
};
