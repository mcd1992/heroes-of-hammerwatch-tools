// http://www.iusmentis.com/technology/hashfunctions/md5/
// http://albumshaper.sourceforge.net/doxygen/md5_8h-source.html
// http://albumshaper.sourceforge.net/doxygen/md5_8cpp-source.html

struct md5 {
// start offset
/* 0x00 */ char state_a[4];
/* 0x04 */ char state_b[4];
/* 0x08 */ char state_c[4];
/* 0x0C */ char state_d[4];

/* 0x10 */ int count_0;
/* 0x14 */ int count_1;

/* 0x18 */ char buffer[64];
/* 0x58 */ char digest[16];
/* 0x68 */ char finalized; // cmp byte [rdi + 0x68], 0 if >0 then MD5::update:  Can't update a finalized digest! error

// Afterwards is a string of the asset
};
