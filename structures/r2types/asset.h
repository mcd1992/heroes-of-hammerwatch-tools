/*
|    ::::   0x004c416f      bf28000000     mov edi, 0x28               ; '(' ; 40
|    ::::   0x004c4174      e8877df4ff     call sym.operatornew_unsignedlong ;[1]
|    ::::   0x004c4179      4885c0         test rax, rax
|   ,=====< 0x004c417c      7425           je 0x4c41a3                 ;[2]
|   |::::   0x004c417e      48c700000000.  mov qword [rax], 0
|   |::::   0x004c4185      44896008       mov dword [rax + 8], r12d
|   |::::   0x004c4189      c74010000000.  mov dword [rax + 0x10], 0
|   |::::   0x004c4190      c6401400       mov byte [rax + 0x14], 0
|   |::::   0x004c4194      c74018000000.  mov dword [rax + 0x18], 0
|   |::::   0x004c419b      48c740200000.  mov qword [rax + 0x20], 0

https://github.com/aappleby/smhasher/blob/master/src/MurmurHash3.cpp#L94
*/

struct asset {
// start offset
/* 0x00 */ uint64_t unk1;
/* 0x08 */ char     murmur_hash[8];
/* 0x10 */ int      asset_packed_size;
/* 0x14 */ uint8_t  asset_type;
/* 0x15 */ char     ignore[3];
/* 0x18 */ void*    asset_data;
/* 0x20 */ uint64_t asset_unpacked_size;
};
