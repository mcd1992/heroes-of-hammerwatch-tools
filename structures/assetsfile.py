# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
from kaitaistruct import __version__ as ks_version, KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if parse_version(ks_version) < parse_version('0.7'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.7 or later is required, but you have %s" % (ks_version))

class Assetsfile(KaitaiStruct):

    class AssetType(Enum):
        unk_0 = 0
        text_1 = 1
        unk_2 = 2
        png_3 = 3

    class MagicHeaders(Enum):
        ssbd = 1347568706
        hoh = 1381128008
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.fileheader = self._root.Fileheader(self._io, self, self._root)
        self.assets = []
        i = 0
        while not self._io.is_eof():
            self.assets.append(self._root.Assetheader(self._io, self, self._root))
            i += 1


    class Fileheader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.magic = self._root.MagicHeaders(self._io.read_u4le())
            self.formatversion = self._io.read_u1()
            self.md5 = self._io.read_bytes(4)
            self.assetscountv1 = self._io.read_u4le()
            if self.formatversion > 1:
                self.assetscount = self._io.read_u4le()



    class Assetheader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.assetnamelen = self._io.read_u4le()
            self.assetnamestr = (self._io.read_bytes(self.assetnamelen)).decode(u"ASCII")
            self.assettype = self._root.AssetType(self._io.read_u1())
            self.unpackedsize = self._io.read_u4le()
            self.packedsize = self._io.read_u4le()
            self.assetdata = self._io.read_bytes((self.packedsize if self.packedsize > 0 else self.unpackedsize))



