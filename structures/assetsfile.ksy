meta:
  id: assetsfile
  file-extension: bin
  endian: le
seq:
  - id: fileheader
    type: fileheader
  - id: assets
    type: assetheader
    repeat: eos

enums:
  asset_type:
    0: unk_0
    1: text_1 # Everything else
    2: unk_2
    3: png_3  # .png and .lvl
  magic_headers:
    0x52525748: 'hoh'  # HWRR (Heroes of Hammerwatch Asset File)
    0x50524442: 'ssbd' # BDRP (Serious Sam: Bogus Detour Asset File)

types:
  fileheader:
    seq:
      - id: magic
        type: u4
        enum: magic_headers
      - id: formatversion
        type: u1
      - id: md5
        size: 4
      - id: assetscountv1
        type: u4
      - id: assetscount
        type: u4
        if: formatversion > 1
  assetheader:
    seq:
      - id: assetnamelen
        type: u4
      - id: assetnamestr
        size: assetnamelen
        type: str
        encoding: ASCII
      - id: assettype # This field is so weird. Could be loader type or maybe load order?
        type: u1
        enum: asset_type
      - id: unpackedsize
        type: u4
      - id: packedsize
        type: u4
      - id: assetdata
        size: '(packedsize > 0) ? packedsize : unpackedsize'
