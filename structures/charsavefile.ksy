meta:
  id: charsavefile
  file-extension: sav
seq:
  - id: header
    type: header
types:
  header:
    seq:
      - id: magic
        contents: 'HHCR'
