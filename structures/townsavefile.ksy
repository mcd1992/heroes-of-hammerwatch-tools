meta:
  id: townsavefile
  file-extension: bsa
seq:
  - id: header
    type: header
types:
  header:
    seq:
      - id: magic
        contents: 'HHTR'
