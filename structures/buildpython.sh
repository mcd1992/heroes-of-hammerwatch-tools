#!/bin/bash
DIRNAME="$(dirname "$0")"

kaitai-struct-compiler --target=python --outdir=$DIRNAME/ $DIRNAME/*.ksy
