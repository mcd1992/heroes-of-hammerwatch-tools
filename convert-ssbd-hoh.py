#!/bin/env python3
# Convert between SSBD and HOHv2 assets.bin headers

import sys
import os
import pathlib
from IPython import embed
from IPython.lib.pretty import pretty
from binascii import hexlify

from structures.assetsfile import Assetsfile

HOHv2_HEADER_SIZE = 17 # bytes
SSBD_HEADER_SIZE = 13 # bytes

targetfile = sys.argv[1]

try:
    assets = Assetsfile.from_file(targetfile)
except EOFError as e:
    print("EOFError", e)

if assets.fileheader.magic.name == 'hoh':
    if assets.fileheader.formatversion == 1:
        print("HOHv1 unsupported")
    else:
        print("HOHv2 header found! Converting to SSBD")
        new_header = b"BDRP" + b"\x01" # Change magic & formatversion
        new_header += assets.fileheader.md5 # Copy md5
        new_header += assets.fileheader.assetscount.to_bytes(4, 'little') # Remove extra assetscount u4, keep original

        # Slice file and remove HOHv2 header
        with open(targetfile, "rb") as assetsbin:
            with open("assetsdata.bin", "wb") as assetsdata:
                assetsdata.write(assetsbin.read()[HOHv2_HEADER_SIZE:])

        # Insert new SSBD header at start of file
        with open("assetsdata.bin", "rb") as assetsdata:
            with open(targetfile, "wb") as newbin:
                newbin.write(new_header)
                newbin.write(assetsdata.read())
        os.remove("assetsdata.bin")

if assets.fileheader.magic.name == 'ssbd':
    print("SSBD header found! Converting to HOHv2")
    new_header = b"HWRR" + b"\x02" # Change magic & formatversion
    new_header += assets.fileheader.md5 # Copy md5
    new_header += b"\x00\x00\x00\x00" # Add extra / blank assetscount
    new_header += assets.fileheader.assetscountv1.to_bytes(4, 'little') # assetscount
    #print( hexlify(new_header).decode('UTF8') )

    # Slice file and remove SSBD header
    with open(targetfile, "rb") as assetsbin:
        with open("assetsdata.bin", "wb") as assetsdata:
            assetsdata.write(assetsbin.read()[SSBD_HEADER_SIZE:])

    # Insert new HOHv2 header at start of file
    with open("assetsdata.bin", "rb") as assetsdata:
        with open(targetfile, "wb") as newbin:
            newbin.write(new_header)
            newbin.write(assetsdata.read())
    os.remove("assetsdata.bin")

#embed()
