#!/bin/env python3

import sys
import os
import pathlib
from IPython import embed
from IPython.lib.pretty import pretty
from binascii import hexlify

from structures.assetsfile import Assetsfile

targetfile = sys.argv[1]
dumpprefix = sys.argv[2] + '/'

try:
    assets = Assetsfile.from_file(targetfile)
except EOFError as e:
    print("EOFError", e)


for asset in assets.assets:
    filepath = asset.assetnamestr
    filename, extension = os.path.splitext(filepath)
    print(filepath)

    # Dump files
    os.makedirs(dumpprefix + os.path.dirname(filepath), exist_ok=True)
    data = asset.assetdata if (hasattr(asset, 'assetdata')) else asset.assetdata_text
    open(dumpprefix + filepath, 'wb').write(data);

#embed()
