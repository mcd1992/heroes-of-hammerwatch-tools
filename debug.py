#!/bin/env python3

import sys
import os
import pathlib
from IPython import embed
from IPython.lib.pretty import pretty
from binascii import hexlify

from structures.assetsfile import Assetsfile

targetfile = sys.argv[1]

try:
    assets = Assetsfile.from_file(targetfile)
except EOFError as e:
    print("EOFError", e)

typemap = {}
for asset in assets.assets:
    filepath = asset.assetnamestr
    filename, extension = os.path.splitext(filepath)
    print(filepath)
    #data = asset.assetdata
    #dump = hexlify(data[0:24]).decode('UTF8')
    #nicedump = " ".join(dump[i:i+2] for i in range(0, len(dump), 2))
    #print('{0} {1}'.format(nicedump, filepath))

#embed()
