var files =
[
    [ "Errors.h", "Errors_8h.html", [
      [ "IError", "classgalaxy_1_1api_1_1IError.html", "classgalaxy_1_1api_1_1IError" ],
      [ "IUnauthorizedAccessError", "classgalaxy_1_1api_1_1IUnauthorizedAccessError.html", null ],
      [ "IInvalidArgumentError", "classgalaxy_1_1api_1_1IInvalidArgumentError.html", null ],
      [ "IInvalidStateError", "classgalaxy_1_1api_1_1IInvalidStateError.html", null ],
      [ "IRuntimeError", "classgalaxy_1_1api_1_1IRuntimeError.html", null ],
      [ "IErrorManager", "classgalaxy_1_1api_1_1IErrorManager.html", "classgalaxy_1_1api_1_1IErrorManager" ]
    ] ],
    [ "GalaxyApi.h", "GalaxyApi_8h.html", "GalaxyApi_8h" ],
    [ "GalaxyExport.h", "GalaxyExport_8h.html", "GalaxyExport_8h" ],
    [ "GalaxyFactory.h", "GalaxyFactory_8h.html", [
      [ "GalaxyFactory", "classgalaxy_1_1api_1_1GalaxyFactory.html", "classgalaxy_1_1api_1_1GalaxyFactory" ]
    ] ],
    [ "GalaxyID.h", "GalaxyID_8h.html", [
      [ "GalaxyID", "classgalaxy_1_1api_1_1GalaxyID.html", "classgalaxy_1_1api_1_1GalaxyID" ]
    ] ],
    [ "IFriends.h", "IFriends_8h.html", "IFriends_8h" ],
    [ "IGalaxy.h", "IGalaxy_8h.html", [
      [ "IGalaxy", "classgalaxy_1_1api_1_1IGalaxy.html", "classgalaxy_1_1api_1_1IGalaxy" ]
    ] ],
    [ "IListenerRegistrar.h", "IListenerRegistrar_8h.html", "IListenerRegistrar_8h" ],
    [ "ILogger.h", "ILogger_8h.html", [
      [ "ILogger", "classgalaxy_1_1api_1_1ILogger.html", "classgalaxy_1_1api_1_1ILogger" ]
    ] ],
    [ "IMatchmaking.h", "IMatchmaking_8h.html", "IMatchmaking_8h" ],
    [ "INetworking.h", "INetworking_8h.html", "INetworking_8h" ],
    [ "IStats.h", "IStats_8h.html", "IStats_8h" ],
    [ "IUser.h", "IUser_8h.html", "IUser_8h" ],
    [ "IUtils.h", "IUtils_8h.html", "IUtils_8h" ],
    [ "stdint.h", "stdint_8h_source.html", null ]
];