var hierarchy =
[
    [ "GalaxyFactory", "classgalaxy_1_1api_1_1GalaxyFactory.html", null ],
    [ "GalaxyID", "classgalaxy_1_1api_1_1GalaxyID.html", null ],
    [ "IError", "classgalaxy_1_1api_1_1IError.html", [
      [ "IInvalidArgumentError", "classgalaxy_1_1api_1_1IInvalidArgumentError.html", null ],
      [ "IInvalidStateError", "classgalaxy_1_1api_1_1IInvalidStateError.html", null ],
      [ "IRuntimeError", "classgalaxy_1_1api_1_1IRuntimeError.html", null ],
      [ "IUnauthorizedAccessError", "classgalaxy_1_1api_1_1IUnauthorizedAccessError.html", null ]
    ] ],
    [ "IErrorManager", "classgalaxy_1_1api_1_1IErrorManager.html", null ],
    [ "IFriends", "classgalaxy_1_1api_1_1IFriends.html", null ],
    [ "IGalaxy", "classgalaxy_1_1api_1_1IGalaxy.html", null ],
    [ "IGalaxyListener", "classgalaxy_1_1api_1_1IGalaxyListener.html", [
      [ "GalaxyTypeAwareListener< type >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", null ],
      [ "GalaxyTypeAwareListener< ACCESS_TOKEN_CHANGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IAccessTokenListener", "classgalaxy_1_1api_1_1IAccessTokenListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< ACHIEVEMENT_CHANGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IAchievementChangeListener", "classgalaxy_1_1api_1_1IAchievementChangeListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< AUTH >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IAuthListener", "classgalaxy_1_1api_1_1IAuthListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< ENCRYPTED_APP_TICKET_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IEncryptedAppTicketListener", "classgalaxy_1_1api_1_1IEncryptedAppTicketListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< FRIEND_LIST_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IFriendListListener", "classgalaxy_1_1api_1_1IFriendListListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< GAME_JOIN_REQUESTED_LISTENER >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IGameJoinRequestedListener", "classgalaxy_1_1api_1_1IGameJoinRequestedListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LEADERBOARD_ENTRIES_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILeaderboardEntriesRetrieveListener", "classgalaxy_1_1api_1_1ILeaderboardEntriesRetrieveListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LEADERBOARD_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILeaderboardRetrieveListener", "classgalaxy_1_1api_1_1ILeaderboardRetrieveListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LEADERBOARD_SCORE_UPDATE_LISTENER >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILeaderboardScoreUpdateListener", "classgalaxy_1_1api_1_1ILeaderboardScoreUpdateListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LEADERBOARDS_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILeaderboardsRetrieveListener", "classgalaxy_1_1api_1_1ILeaderboardsRetrieveListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_CREATED >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyCreatedListener", "classgalaxy_1_1api_1_1ILobbyCreatedListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_DATA >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyDataListener", "classgalaxy_1_1api_1_1ILobbyDataListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_ENTERED >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyEnteredListener", "classgalaxy_1_1api_1_1ILobbyEnteredListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_LEFT >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyLeftListener", "classgalaxy_1_1api_1_1ILobbyLeftListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_LIST >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyListListener", "classgalaxy_1_1api_1_1ILobbyListListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_MEMBER_STATE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyMemberStateListener", "classgalaxy_1_1api_1_1ILobbyMemberStateListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_MESSAGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyMessageListener", "classgalaxy_1_1api_1_1ILobbyMessageListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< LOBBY_OWNER_CHANGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "ILobbyOwnerChangeListener", "classgalaxy_1_1api_1_1ILobbyOwnerChangeListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< NETWORKING >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "INetworkingListener", "classgalaxy_1_1api_1_1INetworkingListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< OPERATIONAL_STATE_CHANGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IOperationalStateChangeListener", "classgalaxy_1_1api_1_1IOperationalStateChangeListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< OVERLAY_STATE_CHANGE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IOverlayStateChangeListener", "classgalaxy_1_1api_1_1IOverlayStateChangeListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< PERSONA_DATA_CHANGED >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IPersonaDataChangedListener", "classgalaxy_1_1api_1_1IPersonaDataChangedListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< RICH_PRESENCE_CHANGE_LISTENER >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IRichPresenceChangeListener", "classgalaxy_1_1api_1_1IRichPresenceChangeListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< SERVER_NETWORKING >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IServerNetworkingListener", "classgalaxy_1_1api_1_1IServerNetworkingListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< STATS_AND_ACHIEVEMENTS_STORE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IStatsAndAchievementsStoreListener", "classgalaxy_1_1api_1_1IStatsAndAchievementsStoreListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< USER_DATA >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IUserDataListener", "classgalaxy_1_1api_1_1IUserDataListener.html", null ]
      ] ],
      [ "GalaxyTypeAwareListener< USER_STATS_AND_ACHIEVEMENTS_RETRIEVE >", "classgalaxy_1_1api_1_1GalaxyTypeAwareListener.html", [
        [ "IUserStatsAndAchievementsRetrieveListener", "classgalaxy_1_1api_1_1IUserStatsAndAchievementsRetrieveListener.html", null ]
      ] ]
    ] ],
    [ "IListenerRegistrar", "classgalaxy_1_1api_1_1IListenerRegistrar.html", null ],
    [ "ILogger", "classgalaxy_1_1api_1_1ILogger.html", null ],
    [ "IMatchmaking", "classgalaxy_1_1api_1_1IMatchmaking.html", null ],
    [ "INetworking", "classgalaxy_1_1api_1_1INetworking.html", null ],
    [ "IStats", "classgalaxy_1_1api_1_1IStats.html", null ],
    [ "IUser", "classgalaxy_1_1api_1_1IUser.html", null ],
    [ "SelfRegisteringListener< _TypeAwareListener >", "classgalaxy_1_1api_1_1SelfRegisteringListener.html", null ]
];