var IMatchmaking_8h =
[
    [ "ILobbyListListener", "classgalaxy_1_1api_1_1ILobbyListListener.html", "classgalaxy_1_1api_1_1ILobbyListListener" ],
    [ "ILobbyCreatedListener", "classgalaxy_1_1api_1_1ILobbyCreatedListener.html", "classgalaxy_1_1api_1_1ILobbyCreatedListener" ],
    [ "ILobbyEnteredListener", "classgalaxy_1_1api_1_1ILobbyEnteredListener.html", "classgalaxy_1_1api_1_1ILobbyEnteredListener" ],
    [ "ILobbyLeftListener", "classgalaxy_1_1api_1_1ILobbyLeftListener.html", "classgalaxy_1_1api_1_1ILobbyLeftListener" ],
    [ "ILobbyDataListener", "classgalaxy_1_1api_1_1ILobbyDataListener.html", "classgalaxy_1_1api_1_1ILobbyDataListener" ],
    [ "ILobbyMemberStateListener", "classgalaxy_1_1api_1_1ILobbyMemberStateListener.html", "classgalaxy_1_1api_1_1ILobbyMemberStateListener" ],
    [ "ILobbyOwnerChangeListener", "classgalaxy_1_1api_1_1ILobbyOwnerChangeListener.html", "classgalaxy_1_1api_1_1ILobbyOwnerChangeListener" ],
    [ "ILobbyMessageListener", "classgalaxy_1_1api_1_1ILobbyMessageListener.html", "classgalaxy_1_1api_1_1ILobbyMessageListener" ],
    [ "IMatchmaking", "classgalaxy_1_1api_1_1IMatchmaking.html", "classgalaxy_1_1api_1_1IMatchmaking" ],
    [ "GlobalLobbyCreatedListener", "IMatchmaking_8h.html#ga4a51265e52d8427a7a76c51c1ebd9984", null ],
    [ "GlobalLobbyDataListener", "IMatchmaking_8h.html#ga7b73cba535d27e5565e8eef4cec81144", null ],
    [ "GlobalLobbyEnteredListener", "IMatchmaking_8h.html#gabf2ad3c27e8349fa3662a935c7893f8c", null ],
    [ "GlobalLobbyLeftListener", "IMatchmaking_8h.html#ga819326fece7e765b59e2b548c97bbe5f", null ],
    [ "GlobalLobbyListListener", "IMatchmaking_8h.html#ga9ee3592412bb8a71f5d710f936ca4621", null ],
    [ "GlobalLobbyMemberStateListener", "IMatchmaking_8h.html#ga2d41f6b1f6dd12b3dc757db188c8db0a", null ],
    [ "GlobalLobbyMessageListener", "IMatchmaking_8h.html#ga659c9fd389b34e22c3517ab892d435c3", null ],
    [ "GlobalLobbyOwnerChangeListener", "IMatchmaking_8h.html#ga46b29610b1e1cf9ba11ae8567d117587", null ],
    [ "LobbyComparisonType", "IMatchmaking_8h.html#gaaa1f784857e2accc0bb6e9ff04f6730d", [
      [ "LOBBY_COMPARISON_TYPE_EQUAL", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730da480f87abf8713dc5e0db8edc91a82b08", null ],
      [ "LOBBY_COMPARISON_TYPE_NOT_EQUAL", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730dadf5dc535e169ab9a847b084577cd9d63", null ],
      [ "LOBBY_COMPARISON_TYPE_GREATER", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730da150cfb626a3aac85a979ed2049878989", null ],
      [ "LOBBY_COMPARISON_TYPE_GREATER_OR_EQUAL", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730daa3371a1534252ee455c86f1cd1cfe59a", null ],
      [ "LOBBY_COMPARISON_TYPE_LOWER", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730da11916e40d4813a04f2209aef2b0eb8a7", null ],
      [ "LOBBY_COMPARISON_TYPE_LOWER_OR_EQUAL", "IMatchmaking_8h.html#ggaaa1f784857e2accc0bb6e9ff04f6730daa8ee60d541753ef47e3cd7fc4b0ab688", null ]
    ] ],
    [ "LobbyCreateResult", "IMatchmaking_8h.html#ga4dbcb3a90897b7a6f019fb313cef3c12", [
      [ "LOBBY_CREATE_RESULT_SUCCESS", "IMatchmaking_8h.html#gga4dbcb3a90897b7a6f019fb313cef3c12ae34b05b10da4c91b3c768ea284987bda", null ],
      [ "LOBBY_CREATE_RESULT_ERROR", "IMatchmaking_8h.html#gga4dbcb3a90897b7a6f019fb313cef3c12a2305d918f65d5a59636c3c6dd38c74ca", null ]
    ] ],
    [ "LobbyEnterResult", "IMatchmaking_8h.html#ga9a697cb4c70ba145451e372d7b064336", [
      [ "LOBBY_ENTER_RESULT_SUCCESS", "IMatchmaking_8h.html#gga9a697cb4c70ba145451e372d7b064336a76b563ecea3fd53da962dd4616190b65", null ],
      [ "LOBBY_ENTER_RESULT_LOBBY_DOES_NOT_EXIST", "IMatchmaking_8h.html#gga9a697cb4c70ba145451e372d7b064336a185be0fbec90a6d7d1ce4fb4be63ef89", null ],
      [ "LOBBY_ENTER_RESULT_LOBBY_IS_FULL", "IMatchmaking_8h.html#gga9a697cb4c70ba145451e372d7b064336a98a29aa953dfbb0b716da9243c20f44f", null ],
      [ "LOBBY_ENTER_RESULT_ERROR", "IMatchmaking_8h.html#gga9a697cb4c70ba145451e372d7b064336af4eab26f860d54c0de9271a144dcd900", null ]
    ] ],
    [ "LobbyMemberStateChange", "IMatchmaking_8h.html#ga8c5f2e74526169399ff2edcfd2d386df", [
      [ "LOBBY_MEMBER_STATE_CHANGED_ENTERED", "IMatchmaking_8h.html#gga8c5f2e74526169399ff2edcfd2d386dfad3bd316a9abdfef0fab174bcabea6736", null ],
      [ "LOBBY_MEMBER_STATE_CHANGED_LEFT", "IMatchmaking_8h.html#gga8c5f2e74526169399ff2edcfd2d386dfa1e1a5fc4eea4d7b53d3386e6ff9fb8a7", null ],
      [ "LOBBY_MEMBER_STATE_CHANGED_DISCONNECTED", "IMatchmaking_8h.html#gga8c5f2e74526169399ff2edcfd2d386dfa0780ded5ee628fcb6a05420844fca0d0", null ],
      [ "LOBBY_MEMBER_STATE_CHANGED_KICKED", "IMatchmaking_8h.html#gga8c5f2e74526169399ff2edcfd2d386dfa0e2f95876ec6b48e87c69c9724eb31ea", null ],
      [ "LOBBY_MEMBER_STATE_CHANGED_BANNED", "IMatchmaking_8h.html#gga8c5f2e74526169399ff2edcfd2d386dfa465cf5af6a4c53c3d6df74e7613996d5", null ]
    ] ],
    [ "LobbyType", "IMatchmaking_8h.html#ga311c1e3b455f317f13d825bb5d775705", [
      [ "LOBBY_TYPE_PUBLIC", "IMatchmaking_8h.html#gga311c1e3b455f317f13d825bb5d775705a62ed4def75b04f5d9bec9ddff877d2b6", null ]
    ] ]
];