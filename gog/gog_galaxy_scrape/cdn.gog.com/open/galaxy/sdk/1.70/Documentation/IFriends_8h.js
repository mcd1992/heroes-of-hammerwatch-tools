var IFriends_8h =
[
    [ "IPersonaDataChangedListener", "classgalaxy_1_1api_1_1IPersonaDataChangedListener.html", "classgalaxy_1_1api_1_1IPersonaDataChangedListener" ],
    [ "IFriendListListener", "classgalaxy_1_1api_1_1IFriendListListener.html", "classgalaxy_1_1api_1_1IFriendListListener" ],
    [ "IRichPresenceChangeListener", "classgalaxy_1_1api_1_1IRichPresenceChangeListener.html", "classgalaxy_1_1api_1_1IRichPresenceChangeListener" ],
    [ "IGameJoinRequestedListener", "classgalaxy_1_1api_1_1IGameJoinRequestedListener.html", "classgalaxy_1_1api_1_1IGameJoinRequestedListener" ],
    [ "IFriends", "classgalaxy_1_1api_1_1IFriends.html", "classgalaxy_1_1api_1_1IFriends" ],
    [ "GlobalFriendListListener", "IFriends_8h.html#ga3c47abf20e566dc0cd0b8bcbc3bc386d", null ],
    [ "GlobalGameJoinRequestedListener", "IFriends_8h.html#gadab0159681deec55f64fa6d1df5d543c", null ],
    [ "GlobalPersonaDataChangedListener", "IFriends_8h.html#gad9c8e014e3ae811c6bd56b7c5b3704f6", null ],
    [ "GlobalRichPresenceChangeListener", "IFriends_8h.html#ga76d003ff80cd39b871a45886762ee2b0", null ],
    [ "AvatarType", "IFriends_8h.html#ga51ba11764e2176ad2cc364635fac294e", [
      [ "SMALL", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294eaea5e596a553757a677cb4da4c8a1f935", null ],
      [ "SMALL_HIGH_RESOLUTION", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294ea26f00c307327630d6c1f1c8798fa6963", null ],
      [ "MEDIUM", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294ea5340ec7ecef6cc3886684a3bd3450d64", null ],
      [ "MEDIUM_HIGH_RESOLUTION", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294eaba296ffdeefedfee874e2069e1c2b532", null ],
      [ "LARGE", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294ea716db5c72140446e5badac4683610310", null ],
      [ "LARGE_HIGH_RESOLUTION", "IFriends_8h.html#gga51ba11764e2176ad2cc364635fac294ea1001ed2c1aa5925063d921fa82b7c94a", null ]
    ] ]
];