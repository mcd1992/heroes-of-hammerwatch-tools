var GalaxyApi_8h =
[
    [ "Friends", "GalaxyApi_8h.html#ga0f0fe7e680ea6179835e830eda0364dc", null ],
    [ "GetError", "GalaxyApi_8h.html#ga64abe9096062430c2930150ab8b9c09b", null ],
    [ "Init", "GalaxyApi_8h.html#ga8debb201f492914f247bf7792cb026da", null ],
    [ "InitLocal", "GalaxyApi_8h.html#ga1445a900d26f318c64abf6f056001f80", null ],
    [ "ListenerRegistrar", "GalaxyApi_8h.html#ga2a74d46371eae788ee979728846c766a", null ],
    [ "Logger", "GalaxyApi_8h.html#ga7af07d2d24e966bf3b58653b170f3544", null ],
    [ "Matchmaking", "GalaxyApi_8h.html#ga907b20a828dccbda7c4fa8358c2c2268", null ],
    [ "Networking", "GalaxyApi_8h.html#ga9d8e206cd518330f81f73a7bd70f9082", null ],
    [ "ProcessData", "GalaxyApi_8h.html#gabc442e3920390455b14786948a88b6ca", null ],
    [ "ServerNetworking", "GalaxyApi_8h.html#ga717322544e0b33383fd39c6960e94bfe", null ],
    [ "Shutdown", "GalaxyApi_8h.html#ga432fe73652cae8bb082b234557a515f2", null ],
    [ "Stats", "GalaxyApi_8h.html#ga4729d0bf087c64c148c6a3c54876aca7", null ],
    [ "User", "GalaxyApi_8h.html#gaaa83a7e89f3601bdc37fbfe3a905e342", null ]
];