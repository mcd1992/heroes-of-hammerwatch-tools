var INetworking_8h =
[
    [ "INetworkingListener", "classgalaxy_1_1api_1_1INetworkingListener.html", "classgalaxy_1_1api_1_1INetworkingListener" ],
    [ "IServerNetworkingListener", "classgalaxy_1_1api_1_1IServerNetworkingListener.html", "classgalaxy_1_1api_1_1IServerNetworkingListener" ],
    [ "INetworking", "classgalaxy_1_1api_1_1INetworking.html", "classgalaxy_1_1api_1_1INetworking" ],
    [ "GlobalNetworkingListener", "INetworking_8h.html#ga5a806b459fd86ecf340e3f258e38fe18", null ],
    [ "GlobalServerNetworkingListener", "INetworking_8h.html#ga4fb33946b6fbfef879cc893aeb1dcb6f", null ],
    [ "P2PSendType", "INetworking_8h.html#gac2b75cd8111214499aef51563ae89f9a", [
      [ "P2P_SEND_UNRELIABLE", "INetworking_8h.html#ggac2b75cd8111214499aef51563ae89f9aaa8b3226f86393bf9a7ac17031c36643c", null ],
      [ "P2P_SEND_RELIABLE", "INetworking_8h.html#ggac2b75cd8111214499aef51563ae89f9aa29b17108439f84ca97abd881a695fb06", null ]
    ] ]
];