#ifndef GALAXY_I_UTILS_H
#define GALAXY_I_UTILS_H

#include "IListenerRegistrar.h"

namespace galaxy
{
    namespace api
    {
        class IOverlayStateChangeListener : public GalaxyTypeAwareListener<OVERLAY_STATE_CHANGE>
        {
        public:

            virtual void OnOverlayStateChanged(bool overlayIsActive) = 0;
        };

        typedef SelfRegisteringListener<IOverlayStateChangeListener> GlobalOverlayStateChangeListener;

    }
}

#endif
