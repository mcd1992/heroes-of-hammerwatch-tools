#ifndef GALAXY_API_H
#define GALAXY_API_H

namespace galaxy
{
    namespace api
    {
    }
}

#include "IGalaxy.h"
#include "IUtils.h"
#include "IUser.h"
#include "IFriends.h"
#include "IMatchmaking.h"
#include "INetworking.h"
#include "IStats.h"
#include "IListenerRegistrar.h"
#include "ILogger.h"
#include "Errors.h"
#include "GalaxyFactory.h"

#include <cstddef>

namespace galaxy
{
    namespace api
    {
        static void GALAXY_CALLTYPE Init(const char* clientID, const char* clientSecret, bool throwExceptions = true)
        {
            try
            {
                GalaxyFactory::CreateInstance()->Init(clientID, clientSecret, throwExceptions);
                if (GalaxyFactory::GetErrorManager()->GetLastError())
                    GalaxyFactory::ResetInstance();
                return;
            }
            catch (...)
            {
                GalaxyFactory::ResetInstance();
                throw;
            }
        }

        static void GALAXY_CALLTYPE InitLocal(const char* clientID, const char* clientSecret, const char* galaxyPeerPath = ".", bool throwExceptions = true)
        {
            try
            {
                GalaxyFactory::CreateInstance()->InitLocal(clientID, clientSecret, galaxyPeerPath, throwExceptions);
                if (GalaxyFactory::GetErrorManager()->GetLastError())
                    GalaxyFactory::ResetInstance();
                return;
            }
            catch (...)
            {
                GalaxyFactory::ResetInstance();
                throw;
            }
        }

        static void GALAXY_CALLTYPE Shutdown()
        {
            if (!GalaxyFactory::GetInstance())
                return;

            GalaxyFactory::GetInstance()->Shutdown();
            GalaxyFactory::ResetInstance();
        }

        static void GALAXY_CALLTYPE ProcessData()
        {
            if (!GalaxyFactory::GetInstance())
                return;

            GalaxyFactory::GetInstance()->ProcessData();
        }

        static IUser* GALAXY_CALLTYPE User()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetUser();
        }

        static IFriends* GALAXY_CALLTYPE Friends()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetFriends();
        }

        static IMatchmaking* GALAXY_CALLTYPE Matchmaking()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetMatchmaking();
        }

        static INetworking* GALAXY_CALLTYPE Networking()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetNetworking();
        }

        static INetworking* GALAXY_CALLTYPE ServerNetworking()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetServerNetworking();
        }

        static IStats* GALAXY_CALLTYPE Stats()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetStats();
        }

        static IListenerRegistrar* GALAXY_CALLTYPE ListenerRegistrar()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetListenerRegistrar();
        }

        static ILogger* GALAXY_CALLTYPE Logger()
        {
            if (!GalaxyFactory::GetInstance())
                return NULL;

            return GalaxyFactory::GetInstance()->GetLogger();
        }

        static const IError* GALAXY_CALLTYPE GetError()
        {
            return GalaxyFactory::GetErrorManager()->GetLastError();
        }

    }
}

#endif
