#ifndef GALAXY_I_GALAXY_H
#define GALAXY_I_GALAXY_H

#include "GalaxyExport.h"

namespace galaxy
{
    namespace api
    {
        class IUser;
        class IFriends;
        class IMatchmaking;
        class INetworking;
        class IStats;
        class IListenerRegistrar;
        class ILogger;
        class IError;

        class GALAXY_DLL_EXPORT IGalaxy
        {
        public:

            virtual ~IGalaxy()
            {
            }

            virtual void Init(const char* clientID, const char* clientSecret, bool throwExceptions = true) = 0;

            virtual void InitLocal(const char* clientID, const char* clientSecret, const char* galaxyPeerPath = ".", bool throwExceptions = true) = 0;

            virtual void Shutdown() = 0;

            virtual IUser* GetUser() const = 0;

            virtual IFriends* GetFriends() const = 0;

            virtual IMatchmaking* GetMatchmaking() const = 0;

            virtual INetworking* GetNetworking() const = 0;

            virtual INetworking* GetServerNetworking() const = 0;

            virtual IStats* GetStats() const = 0;

            virtual IListenerRegistrar* GetListenerRegistrar() const = 0;

            virtual ILogger* GetLogger() const = 0;

            virtual void ProcessData() = 0;

            virtual const IError* GetError() const = 0;
        };

    }
}

#endif
