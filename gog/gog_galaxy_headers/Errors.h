#ifndef GALAXY_ERRORS_H
#define GALAXY_ERRORS_H

namespace galaxy
{
    namespace api
    {
        class IError
        {
        public:

            virtual ~IError()
            {
            }

            virtual const char* GetName() const = 0;

            virtual const char* GetMsg() const = 0;

            enum Type
            {
                UNAUTHORIZED_ACCESS,
                INVALID_ARGUMENT,
                INVALID_STATE,
                RUNTIME_ERROR
            };

            virtual Type GetType() const = 0;
        };

        class IUnauthorizedAccessError : public IError
        {
        };

        class IInvalidArgumentError : public IError
        {
        };

        class IInvalidStateError : public IError
        {
        };

        class IRuntimeError : public IError
        {
        };

        class IErrorManager
        {
        public:

            virtual ~IErrorManager()
            {
            }

            virtual api::IError* GetLastError() = 0;
        };

    }
}

#endif
