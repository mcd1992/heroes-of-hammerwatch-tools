#ifndef GALAXY_GALAXY_FACTORY_H
#define GALAXY_GALAXY_FACTORY_H

#include "IGalaxy.h"
#include "Errors.h"
#include "GalaxyExport.h"

namespace galaxy
{
    namespace api
    {
        class GALAXY_DLL_EXPORT GalaxyFactory
        {
        public:

            static IGalaxy* GALAXY_CALLTYPE GetInstance();

            static IErrorManager* GALAXY_CALLTYPE GetErrorManager();

            static void GALAXY_CALLTYPE ResetInstance();

            static IGalaxy* GALAXY_CALLTYPE CreateInstance();

        private:

            static IGalaxy* instance;
            static IErrorManager* errorManager;
        };

    }
}

#endif
