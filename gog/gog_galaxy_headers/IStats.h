#ifndef GALAXY_I_STATS_H
#define GALAXY_I_STATS_H

#include "IListenerRegistrar.h"
#include "GalaxyID.h"

namespace galaxy
{
    namespace api
    {

        enum LeaderboardSortMethod
        {
            LEADERBOARD_SORT_METHOD_NONE = 0,
            LEADERBOARD_SORT_METHOD_ASCENDING = 1,
            LEADERBOARD_SORT_METHOD_DESCENDING = 2
        };

        enum LeaderboardDisplayType
        {
            LEADERBOARD_DISPLAY_TYPE_NONE = 0,
            LEADERBOARD_DISPLAY_TYPE_NUMBER = 1,
            LEADERBOARD_DISPLAY_TYPE_TIME_SECONDS = 2,
            LEADERBOARD_DISPLAY_TYPE_TIME_MILLISECONDS = 3
        };

        class IUserStatsAndAchievementsRetrieveListener : public GalaxyTypeAwareListener<USER_STATS_AND_ACHIEVEMENTS_RETRIEVE>
        {
        public:

            virtual void OnUserStatsAndAchievementsRetrieveSuccess(GalaxyID userID) = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnUserStatsAndAchievementsRetrieveFailure(GalaxyID userID, FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<IUserStatsAndAchievementsRetrieveListener> GlobalUserStatsAndAchievementsRetrieveListener;

        class IStatsAndAchievementsStoreListener : public GalaxyTypeAwareListener<STATS_AND_ACHIEVEMENTS_STORE>
        {
        public:

            virtual void OnUserStatsAndAchievementsStoreSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnUserStatsAndAchievementsStoreFailure(FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<IStatsAndAchievementsStoreListener> GlobalStatsAndAchievementsStoreListener;

        class IAchievementChangeListener : public GalaxyTypeAwareListener<ACHIEVEMENT_CHANGE>
        {
        public:

            // // /**
            // //  * Notification for the event of changing progress in unlocking
            // //  * a particular achievement.
            // //  *
            // //  * @param name The code name of the achievement.
            // //  * @param currentProgress Current value of progress for the achievement.
            // //  * @param maxProgress The maximum value of progress for the achievement.
            // //  */
            // // void OnAchievementProgressChanged(const char* name, uint32_t currentProgress, uint32_t maxProgress) = 0;

            virtual void OnAchievementUnlocked(const char* name) = 0;
        };

        typedef SelfRegisteringListener<IAchievementChangeListener> GlobalAchievementChangeListener;

        class ILeaderboardsRetrieveListener : public GalaxyTypeAwareListener<LEADERBOARDS_RETRIEVE>
        {
        public:

            virtual void OnLeaderboardsRetrieveSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnLeaderboardsRetrieveFailure(FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<ILeaderboardsRetrieveListener> GlobalLeaderboardsRetrieveListener;

        class ILeaderboardEntriesRetrieveListener : public GalaxyTypeAwareListener<LEADERBOARD_ENTRIES_RETRIEVE>
        {
        public:

            virtual void OnLeaderboardEntriesRetrieveSuccess(const char* name, uint32_t entryCount) = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED,
                FAILURE_REASON_NOT_FOUND
            };

            virtual void OnLeaderboardEntriesRetrieveFailure(const char* name, FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<ILeaderboardEntriesRetrieveListener> GlobalLeaderboardEntriesRetrieveListener;

        class ILeaderboardScoreUpdateListener : public GalaxyTypeAwareListener<LEADERBOARD_SCORE_UPDATE_LISTENER>
        {
        public:

            virtual void OnLeaderboardScoreUpdateSuccess(const char* name, int32_t score, uint32_t oldRank, uint32_t newRank) = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED,
                FAILURE_REASON_NO_IMPROVEMENT
            };

            virtual void OnLeaderboardScoreUpdateFailure(const char* name, int32_t score, FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<ILeaderboardScoreUpdateListener> GlobalLeaderboardScoreUpdateListener;

        class ILeaderboardRetrieveListener : public GalaxyTypeAwareListener<LEADERBOARD_RETRIEVE>
        {
        public:

            virtual void OnLeaderboardRetrieveSuccess(const char* name) = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnLeaderboardRetrieveFailure(const char* name, FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<ILeaderboardRetrieveListener> GlobalLeaderboardRetrieveListener;

        class IStats
        {
        public:

            virtual ~IStats()
            {
            }

            virtual void RequestUserStatsAndAchievements(GalaxyID userID = GalaxyID()) = 0;

            virtual int32_t GetStatInt(const char* name, GalaxyID userID = GalaxyID()) = 0;

            virtual float GetStatFloat(const char* name, GalaxyID userID = GalaxyID()) = 0;

            virtual void SetStatInt(const char* name, int32_t value) = 0;

            virtual void SetStatFloat(const char* name, float value) = 0;

            virtual void UpdateAvgRateStat(const char* name, float countThisSession, double sessionLength) = 0;

            virtual void GetAchievement(const char* name, bool& unlocked, uint32_t& unlockTime, GalaxyID userID = GalaxyID()) = 0;

            virtual void SetAchievement(const char* name) = 0;

            virtual void ClearAchievement(const char* name) = 0;

            virtual void StoreStatsAndAchievements() = 0;

            virtual void ResetStatsAndAchievements() = 0;

            virtual const char* GetAchievementDisplayName(const char* name) = 0;

            virtual const char* GetAchievementDescription(const char* name) = 0;

            virtual bool IsAchievementVisible(const char* name) = 0;

            virtual void RequestLeaderboards() = 0;

            virtual const char* GetLeaderboardDisplayName(const char* name) = 0;

            virtual LeaderboardSortMethod GetLeaderboardSortMethod(const char* name) = 0;

            virtual LeaderboardDisplayType GetLeaderboardDisplayType(const char* name) = 0;

            virtual void RequestLeaderboardEntriesGlobal(const char* name, uint32_t rangeStart, uint32_t rangeEnd) = 0;

            virtual void RequestLeaderboardEntriesAroundUser(const char* name, uint32_t countBefore, uint32_t countAfter, GalaxyID userID = GalaxyID()) = 0;

            virtual void RequestLeaderboardEntriesForUsers(const char* name, GalaxyID* userArray, uint32_t userArraySize) = 0;

            virtual void GetRequestedLeaderboardEntry(uint32_t index, uint32_t& rank, int32_t& score, GalaxyID& userID) = 0;

            virtual void GetRequestedLeaderboardEntryWithDetails(uint32_t index, uint32_t& rank, int32_t& score, void* details, uint32_t detailsSize, uint32_t& outDetailsSize, GalaxyID& userID) = 0;

            virtual void SetLeaderboardScore(const char* name, int32_t score, bool forceUpdate = false) = 0;

            virtual void SetLeaderboardScoreWithDetails(const char* name, int32_t score, const void* details, uint32_t detailsSize, bool forceUpdate = false) = 0;

            virtual uint32_t GetLeaderboardEntryCount(const char* name) = 0;

            virtual void FindLeaderboard(const char* name) = 0;

            virtual void FindOrCreateLeaderboard(const char* name, const char* displayName, const LeaderboardSortMethod& sortMethod, const LeaderboardDisplayType& displayType) = 0;
        };

    }
}

#endif
