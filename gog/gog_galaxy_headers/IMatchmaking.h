#ifndef GALAXY_I_MATCHMAKING_H
#define GALAXY_I_MATCHMAKING_H

#include "IUser.h"
#include "IListenerRegistrar.h"

namespace galaxy
{
    namespace api
    {
        enum LobbyType
        {
            //LOBBY_TYPE_PRIVATE = 0, ///< Only invited users are able to join the lobby.
            //LOBBY_TYPE_FRIENDS_ONLY = 1, ///< Visible only to friends or invitees, but not in lobby list.
            LOBBY_TYPE_PUBLIC = 2
            //LOBBY_TYPE_INVISIBLE = 3 ///< Returned by search, but not visible to friends.
        };

        enum LobbyMemberStateChange
        {
            LOBBY_MEMBER_STATE_CHANGED_ENTERED = 0x0001,
            LOBBY_MEMBER_STATE_CHANGED_LEFT = 0x0002,
            LOBBY_MEMBER_STATE_CHANGED_DISCONNECTED = 0x0004,
            LOBBY_MEMBER_STATE_CHANGED_KICKED = 0x0008,
            LOBBY_MEMBER_STATE_CHANGED_BANNED = 0x0010
        };

        enum LobbyComparisonType
        {
            LOBBY_COMPARISON_TYPE_EQUAL = 0,
            LOBBY_COMPARISON_TYPE_NOT_EQUAL = 1,
            LOBBY_COMPARISON_TYPE_GREATER = 2,
            LOBBY_COMPARISON_TYPE_GREATER_OR_EQUAL = 3,
            LOBBY_COMPARISON_TYPE_LOWER = 4,
            LOBBY_COMPARISON_TYPE_LOWER_OR_EQUAL = 5
        };

        enum LobbyCreateResult
        {
            LOBBY_CREATE_RESULT_SUCCESS,
            LOBBY_CREATE_RESULT_ERROR
        };

        enum LobbyEnterResult
        {
            LOBBY_ENTER_RESULT_SUCCESS,
            LOBBY_ENTER_RESULT_LOBBY_DOES_NOT_EXIST,
            LOBBY_ENTER_RESULT_LOBBY_IS_FULL,
            LOBBY_ENTER_RESULT_ERROR
        };

        class ILobbyListListener : public GalaxyTypeAwareListener<LOBBY_LIST>
        {
        public:

            virtual void OnLobbyList(uint32_t lobbyCount, bool ioFailure) = 0;
        };

        typedef SelfRegisteringListener<ILobbyListListener> GlobalLobbyListListener;

        class ILobbyCreatedListener : public GalaxyTypeAwareListener<LOBBY_CREATED>
        {
        public:

            virtual void OnLobbyCreated(const GalaxyID& lobbyID, LobbyCreateResult result) = 0;
        };

        typedef SelfRegisteringListener<ILobbyCreatedListener> GlobalLobbyCreatedListener;

        class ILobbyEnteredListener : public GalaxyTypeAwareListener<LOBBY_ENTERED>
        {
        public:

            virtual void OnLobbyEntered(const GalaxyID& lobbyID, LobbyEnterResult result) = 0;
        };

        typedef SelfRegisteringListener<ILobbyEnteredListener> GlobalLobbyEnteredListener;

        class ILobbyLeftListener : public GalaxyTypeAwareListener<LOBBY_LEFT>
        {
        public:

            virtual void OnLobbyLeft(const GalaxyID& lobbyID, bool ioFailure) = 0;
        };

        typedef SelfRegisteringListener<ILobbyLeftListener> GlobalLobbyLeftListener;

        class ILobbyDataListener : public GalaxyTypeAwareListener<LOBBY_DATA>
        {
        public:

            virtual void OnLobbyDataUpdated(const GalaxyID& lobbyID, const GalaxyID& memberID) = 0;
        };

        typedef SelfRegisteringListener<ILobbyDataListener> GlobalLobbyDataListener;

        class ILobbyMemberStateListener : public GalaxyTypeAwareListener<LOBBY_MEMBER_STATE>
        {
        public:

            virtual void OnLobbyMemberStateChanged(const GalaxyID& lobbyID, const GalaxyID& memberID, LobbyMemberStateChange memberStateChange) = 0;
        };

        typedef SelfRegisteringListener<ILobbyMemberStateListener> GlobalLobbyMemberStateListener;

        class ILobbyOwnerChangeListener : public GalaxyTypeAwareListener<LOBBY_OWNER_CHANGE>
        {
        public:

            virtual void OnLobbyOwnerChanged(const GalaxyID& lobbyID, const GalaxyID& newOwnerID) = 0;
        };

        typedef SelfRegisteringListener<ILobbyOwnerChangeListener> GlobalLobbyOwnerChangeListener;

        class ILobbyMessageListener : public GalaxyTypeAwareListener<LOBBY_MESSAGE>
        {
        public:

            virtual void OnLobbyMessageReceived(const GalaxyID& lobbyID, const GalaxyID& senderID, uint32_t messageID, uint32_t messageLength) = 0;
        };

        typedef SelfRegisteringListener<ILobbyMessageListener> GlobalLobbyMessageListener;

        class IMatchmaking
        {
        public:

            virtual ~IMatchmaking()
            {
            }

            virtual void CreateLobby(LobbyType lobbyType, uint32_t maxMembers) = 0;

            virtual void RequestLobbyList() = 0;

            virtual void AddRequestLobbyListStringFilter(const char* keyToMatch, const char* valueToMatch, LobbyComparisonType comparisonType) = 0;

            virtual void AddRequestLobbyListNumericalFilter(const char* keyToMatch, int32_t valueToMatch, LobbyComparisonType comparisonType) = 0;

            virtual void AddRequestLobbyListNearValueFilter(const char* keyToMatch, int32_t valueToBeCloseTo) = 0;

            virtual GalaxyID GetLobbyByIndex(uint32_t index) = 0;

            virtual void JoinLobby(GalaxyID lobbyID) = 0;

            virtual void LeaveLobby(GalaxyID lobbyID) = 0;

            virtual uint32_t GetMaxNumLobbyMembers(GalaxyID lobbyID) = 0;

            virtual uint32_t GetNumLobbyMembers(GalaxyID lobbyID) = 0;

            virtual GalaxyID GetLobbyMemberByIndex(GalaxyID lobbyID, uint32_t index) = 0;

            virtual bool RequestLobbyData(GalaxyID lobbyID) = 0;

            virtual const char* GetLobbyData(GalaxyID lobbyID, const char* key) = 0;

            virtual bool SetLobbyData(GalaxyID lobbyID, const char* key, const char* value) = 0;

            virtual uint32_t GetLobbyDataCount(GalaxyID lobbyID) = 0;

            virtual bool GetLobbyDataByIndex(GalaxyID lobbyID, uint32_t index, char* key, uint32_t keyLength, char* value, uint32_t valueLength) = 0;

            virtual bool DeleteLobbyData(GalaxyID lobbyID, const char* key) = 0;

            virtual const char* GetLobbyMemberData(GalaxyID lobbyID, GalaxyID memberID, const char* key) = 0;

            virtual void SetLobbyMemberData(GalaxyID lobbyID, const char* key, const char* value) = 0;

            virtual uint32_t GetLobbyMemberDataCount(GalaxyID lobbyID, GalaxyID memberID) = 0;

            virtual bool GetLobbyMemberDataByIndex(GalaxyID lobbyID, GalaxyID memberID, uint32_t index, char* key, uint32_t keyLength, char* value, uint32_t valueLength) = 0;

            virtual void DeleteLobbyMemberData(GalaxyID lobbyID, const char* key) = 0;

            virtual GalaxyID GetLobbyOwner(GalaxyID lobbyID) = 0;

            virtual bool SendLobbyMessage(GalaxyID lobbyID, const char* msg, uint32_t msgLength) = 0;

            virtual uint32_t GetLobbyMessage(GalaxyID lobbyID, uint32_t messageID, GalaxyID& senderID, char* msg, uint32_t msgLength) = 0;
        };

    }
}

#endif
