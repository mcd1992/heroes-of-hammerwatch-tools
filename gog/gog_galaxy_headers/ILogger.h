#ifndef GALAXY_I_LOGGER_H
#define GALAXY_I_LOGGER_H

namespace galaxy
{
    namespace api
    {
        class ILogger
        {
        public:

            virtual ~ILogger()
            {
            }

            virtual void Trace(const char* format, ...) = 0;

            virtual void Debug(const char* format, ...) = 0;

            virtual void Info(const char* format, ...) = 0;

            virtual void Warning(const char* format, ...) = 0;

            virtual void Error(const char* format, ...) = 0;

            virtual void Fatal(const char* format, ...) = 0;
        };

    }
}

#endif
