#ifndef GALAXY_I_FRIENDS_H
#define GALAXY_I_FRIENDS_H

#include "GalaxyID.h"
#include "IListenerRegistrar.h"

namespace galaxy
{
    namespace api
    {
        enum AvatarType
        {
            SMALL = 0,
            SMALL_HIGH_RESOLUTION = 1,
            MEDIUM = 2,
            MEDIUM_HIGH_RESOLUTION = 3,
            LARGE = 4,
            LARGE_HIGH_RESOLUTION = 5
        };

        class IPersonaDataChangedListener : public GalaxyTypeAwareListener<PERSONA_DATA_CHANGED>
        {
        public:

            enum PersonaStateChange
            {
                PERSONA_CHANGE_NONE = 0x0000,
                PERSONA_CHANGE_NAME = 0x0001,
                PERSONA_CHANGE_AVATAR = 0x0002
            };

            virtual void OnPersonaDataChanged(GalaxyID userID, uint32_t personaStateChange) = 0;
        };

        typedef SelfRegisteringListener<IPersonaDataChangedListener> GlobalPersonaDataChangedListener;

        class IFriendListListener : public GalaxyTypeAwareListener<FRIEND_LIST_RETRIEVE>
        {
        public:

            virtual void OnFriendListRetrieveSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnFriendListRetrieveFailure(FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<IFriendListListener> GlobalFriendListListener;

        class IRichPresenceChangeListener : public GalaxyTypeAwareListener<RICH_PRESENCE_CHANGE_LISTENER>
        {
        public:

            virtual void OnRichPresenceChangeSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED
            };

            virtual void OnRichPresenceChangeFailure(FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<IRichPresenceChangeListener> GlobalRichPresenceChangeListener;

        class IGameJoinRequestedListener : public GalaxyTypeAwareListener<GAME_JOIN_REQUESTED_LISTENER>
        {
        public:

            virtual void OnGameJoinRequested(GalaxyID userID, const char* connectionString) = 0;
        };

        typedef SelfRegisteringListener<IGameJoinRequestedListener> GlobalGameJoinRequestedListener;

        class IFriends
        {
        public:

            virtual ~IFriends()
            {
            }

            virtual const char* GetPersonaName() = 0;

            virtual void RequestUserInformation(GalaxyID userID) = 0;

            virtual const char* GetFriendPersonaName(GalaxyID userID) = 0;

            virtual const char* GetFriendAvatarUrl(GalaxyID userID, AvatarType avatarType) = 0;

            virtual void RequestFriendList() = 0;

            virtual uint32_t GetFriendCount() = 0;

            virtual GalaxyID GetFriendByIndex(uint32_t friendIndex) = 0;

            virtual void SetRichPresence(const char* key, const char* value) = 0;

            virtual void DeleteRichPresence(const char* key) = 0;

            virtual void ClearRichPresence() = 0;

            virtual void ShowOverlayInviteDialog(const char* connectionString) = 0;
        };

    }
}

#endif
