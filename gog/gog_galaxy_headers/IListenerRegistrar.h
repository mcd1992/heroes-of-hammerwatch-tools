#ifndef GALAXY_I_LISTENER_REGISTRAR_H
#define GALAXY_I_LISTENER_REGISTRAR_H

#include "GalaxyFactory.h"
#include "stdint.h"
#include <stdlib.h>

namespace galaxy
{
    namespace api
    {
        enum ListenerType
        {
            LISTENER_TYPE_BEGIN,
            LOBBY_LIST = LISTENER_TYPE_BEGIN,
            LOBBY_CREATED,
            LOBBY_ENTERED,
            LOBBY_LEFT,
            LOBBY_DATA,
            LOBBY_MEMBER_STATE,
            LOBBY_OWNER_CHANGE,
            AUTH,
            LOBBY_MESSAGE,
            NETWORKING,
            SERVER_NETWORKING,
            USER_DATA,
            USER_STATS_AND_ACHIEVEMENTS_RETRIEVE,
            STATS_AND_ACHIEVEMENTS_STORE,
            ACHIEVEMENT_CHANGE,
            LEADERBOARDS_RETRIEVE,
            LEADERBOARD_ENTRIES_RETRIEVE,
            LEADERBOARD_SCORE_UPDATE_LISTENER,
            PERSONA_DATA_CHANGED,
            RICH_PRESENCE_CHANGE_LISTENER,
            GAME_JOIN_REQUESTED_LISTENER,
            OPERATIONAL_STATE_CHANGE,
            OVERLAY_STATE_CHANGE,
            FRIEND_LIST_RETRIEVE,
            ENCRYPTED_APP_TICKET_RETRIEVE,
            ACCESS_TOKEN_CHANGE,
            LEADERBOARD_RETRIEVE,
            LISTENER_TYPE_END
        };

        class IGalaxyListener
        {
        public:

            virtual ~IGalaxyListener()
            {
            }
        };

        template<uint32_t type> class GalaxyTypeAwareListener : public IGalaxyListener
        {
        public:

            static uint32_t GetListenerType()
            {
                return type;
            }
        };

        class IListenerRegistrar
        {
        public:

            virtual ~IListenerRegistrar()
            {
            }

            virtual void Register(uint32_t listenerType, IGalaxyListener* listener) = 0;

            virtual void Unregister(uint32_t listenerType, IGalaxyListener* listener) = 0;
        };

        template<typename _TypeAwareListener> class SelfRegisteringListener : public _TypeAwareListener
        {
        public:

            SelfRegisteringListener(IListenerRegistrar* _registrar = NULL)
                : registrar(_registrar)
            {
                if (!registrar)
                    registrar = GalaxyFactory::GetInstance()->GetListenerRegistrar();

                registrar->Register(_TypeAwareListener::GetListenerType(), this);
            }

            ~SelfRegisteringListener()
            {
                registrar->Unregister(_TypeAwareListener::GetListenerType(), this);
            }

        private:

            IListenerRegistrar* registrar;
        };

    }
}

#endif
