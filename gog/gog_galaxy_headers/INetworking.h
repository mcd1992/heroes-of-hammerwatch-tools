#ifndef GALAXY_I_NETWORKING_H
#define GALAXY_I_NETWORKING_H

#include "IListenerRegistrar.h"
#include "GalaxyID.h"

namespace galaxy
{
    namespace api
    {
        class INetworkingListener : public GalaxyTypeAwareListener<NETWORKING>
        {
        public:

            virtual void OnP2PPacketAvailable(uint32_t msgSize, uint8_t channel) = 0;
        };

        typedef SelfRegisteringListener<INetworkingListener> GlobalNetworkingListener;

        class IServerNetworkingListener : public GalaxyTypeAwareListener<SERVER_NETWORKING>
        {
        public:

            virtual void OnServerP2PPacketAvailable(uint32_t msgSize, uint8_t channel) = 0;
        };

        typedef SelfRegisteringListener<IServerNetworkingListener> GlobalServerNetworkingListener;

        enum P2PSendType
        {
            P2P_SEND_UNRELIABLE = 0,
            P2P_SEND_RELIABLE = 1
        };

        class INetworking
        {
        public:

            virtual ~INetworking()
            {
            }

            virtual bool SendP2PPacket(GalaxyID galaxyID, const void* data, uint32_t dataLen, P2PSendType sendType, uint8_t channel = 0) = 0;

            virtual bool PeekP2PPacket(void* dest, uint32_t destSize, uint32_t* outMsgSize, GalaxyID& outGalaxyID, uint8_t channel = 0) = 0;

            virtual bool IsP2PPacketAvailable(uint32_t* outMsgSize, uint8_t channel = 0) = 0;

            virtual bool ReadP2PPacket(void* dest, uint32_t destSize, uint32_t* outMsgSize, GalaxyID& outGalaxyID, uint8_t channel = 0) = 0;

            virtual void PopP2PPacket(uint8_t channel = 0) = 0;

            virtual int GetPingWith(GalaxyID galaxyID) = 0;
        };

    }
}

#endif
