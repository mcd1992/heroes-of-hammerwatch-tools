#ifndef GALAXY_I_USER_H
#define GALAXY_I_USER_H

#include "IListenerRegistrar.h"
#include "GalaxyID.h"

namespace galaxy
{
    namespace api
    {
        class IAuthListener : public GalaxyTypeAwareListener<AUTH>
        {
        public:

            virtual void OnAuthSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED = 0,
                FAILURE_REASON_GALAXY_SERVICE_NOT_AVAILABLE = 1,
                FAILURE_REASON_GALAXY_SERVICE_NOT_SIGNED_IN = 2,
                FAILURE_REASON_CONNECTION_FAILURE = 3,
                FAILURE_REASON_NO_LICENSE = 4,
                FAILURE_REASON_INVALID_CREDENTIALS = 5
            };

            virtual void OnAuthFailure(FailureReason failureReason) = 0;

            virtual void OnAuthLost() = 0;
        };

        typedef SelfRegisteringListener<IAuthListener> GlobalAuthListener;

        class IOperationalStateChangeListener : public GalaxyTypeAwareListener<OPERATIONAL_STATE_CHANGE>
        {
        public:

            enum OperationalState
            {
                OPERATIONAL_STATE_SIGNED_IN = 0x0001,
                OPERATIONAL_STATE_LOGGED_ON = 0x0002
            };

            virtual void OnOperationalStateChanged(uint32_t operationalState) = 0;
        };

        typedef SelfRegisteringListener<IOperationalStateChangeListener> GlobalOperationalStateChangeListener;

        class IUserDataListener : public GalaxyTypeAwareListener<USER_DATA>
        {
        public:

            virtual void OnUserDataUpdated() = 0;
        };

        typedef SelfRegisteringListener<IUserDataListener> GlobalUserDataListener;

        class IEncryptedAppTicketListener : public GalaxyTypeAwareListener<ENCRYPTED_APP_TICKET_RETRIEVE>
        {
        public:

            virtual void OnEncryptedAppTicketRetrieveSuccess() = 0;

            enum FailureReason
            {
                FAILURE_REASON_UNDEFINED = 0
            };

            virtual void OnEncryptedAppTicketRetrieveFailure(FailureReason failureReason) = 0;
        };

        typedef SelfRegisteringListener<IEncryptedAppTicketListener> GlobalEncryptedAppTicketListener;

        class IAccessTokenListener : public GalaxyTypeAwareListener<ACCESS_TOKEN_CHANGE>
        {
        public:

            virtual void OnAccessTokenChanged() = 0;
        };

        typedef SelfRegisteringListener<IAccessTokenListener> GlobalAccessTokenListener;

        class IUser
        {
        public:

            virtual ~IUser()
            {
            }

            virtual bool SignedIn() = 0;

            virtual GalaxyID GetGalaxyID() = 0;

            virtual void SignIn(const char* login, const char* password) = 0;

            virtual void SignIn(const char* steamAppTicket, uint32_t steamAppTicketLength, const char* personaName) = 0;

            virtual void SignIn() = 0;

            virtual void RequestUserData() = 0;

            virtual const char* GetUserData(const char* key) = 0;

            virtual void SetUserData(const char* key, const char* value) = 0;

            virtual uint32_t GetUserDataCount() = 0;

            virtual bool GetUserDataByIndex(uint32_t index, char* key, uint32_t keyLength, char* value, uint32_t valueLength) = 0;

            virtual void DeleteUserData(const char* key) = 0;

            virtual bool IsLoggedOn() = 0;

            virtual void RequestEncryptedAppTicket(void* data, uint32_t dataSize) = 0;

            virtual void GetEncryptedAppTicket(void* encryptedAppTicket, uint32_t maxEncryptedAppTicketLength, uint32_t& currentEncryptedAppTicketLength) = 0;

            virtual void SignIn(const char* serverKey) = 0;

            virtual const char* GetAccessToken() = 0;

            virtual bool ReportInvalidAccessToken(const char* accessToken) = 0;
        };

    }
}

#endif
